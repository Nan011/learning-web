from django.contrib import admin

from . import models
# Register your models here.
admin.site.register(models.Project)
admin.site.register(models.Education)
admin.site.register(models.Ability)
admin.site.register(models.AbilityType)
admin.site.register(models.Profile)
admin.site.register(models.Image)
admin.site.register(models.Icon)
admin.site.register(models.Schedule)