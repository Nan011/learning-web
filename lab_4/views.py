from django.shortcuts import render, HttpResponse, redirect
from django.http import HttpResponseRedirect
from django.utils import timezone
from lab_4 import models
from lab_4 import forms 

# Input the data
personal_info = models.Profile.objects.get(name="Nandhika Prayoga")
background_path = models.Image.objects.get(name="main-bg").image.url

def calculate_year(begin_date):
	year = timezone.now().year - begin_date.year
	if begin_date.month > timezone.now().month:
		return year - 1	
	else:
		return year

# Views controller
def get_profile(request):
	contents = {
		"background": background_path,
		"photo": personal_info.photo.image.url,
		"name": personal_info.name, 
		"age": calculate_year(personal_info.birth_date),
		"desc": personal_info.description,
		"instances": models.Education.objects.all(),
		"email": personal_info.email,
	}
	return render(request, "main-pages/profile.html", contents)

def get_projects(request):
	project_query = models.Project.objects.all()
	contents = {
		"background": background_path, 
		"projects": project_query, 
		"file_exists": project_query.first() != None, 
	}
	return render(request, "main-pages/projects.html", contents)

def get_home(request):
	contents = {
		"background": background_path,
	}
	return render(request, "main-pages/home.html", contents)

def get_register(request):
	contents = {
		"background": background_path,
	}
	return render(request, "main-pages/register.html", contents)

def get_schedules(request):
	if request.method == "POST":
		form = forms.ScheduleForm(request.POST)
		if form.is_valid():
			form_name = form.cleaned_data["name"]
			form_category = form.cleaned_data["category"]
			form_place = form.cleaned_data["place"]
			form_start_date = form.cleaned_data["start_date"]
			form_end_date = form.cleaned_data["end_date"]
			new_schedule = models.Schedule(
				name=form_name,
				category=form_category,
				place=form_place,
				start_date=form_start_date,
				end_date=form_end_date
			)
			new_schedule.save()
		return redirect(get_schedules)
	form = forms.ScheduleForm()
	query = models.Schedule.objects.all()
	contents = {
		"background": background_path,
		"form": form,
		"schedules": query,
		"file_exists": query.first() != None,
		"list_icon": models.Icon.objects.get(name="list").icon.url,
		"place_icon": models.Icon.objects.get(name="placeholder").icon.url,
		"cal_icon": models.Icon.objects.get(name="Calender").icon.url,
		"close_icon": models.Icon.objects.get(name="close").icon.url,
		"event_icon": models.Icon.objects.get(name="event").icon.url, 
	}	
	return render(request, "main-pages/schedules.html", contents)

def delete_all_schedule(request):
	models.Schedule.objects.all().delete()
	return redirect(get_schedules)

def delete_schedule(request, id):
	models.Schedule.objects.get(id=id).delete()
	return redirect(get_schedules)