from django import forms
from django.utils import timezone
from . import models

class ScheduleForm(forms.Form):
	name = forms.CharField(widget=forms.TextInput(
		attrs={
			"class": "green white-font"
		}
	))
	category = forms.ChoiceField(choices = models.ScheduleCategory.CATEGORY_CHOICES,
		widget=forms.Select(
		attrs={
			"class": "green white-font"
		}
	))
	place = forms.CharField(widget=forms.TextInput(
		attrs={
			"class": "green white-font"
		}
	))
	start_date = forms.DateTimeField(initial=timezone.now(), widget=forms.TextInput(
		attrs={
			"class": "green white-font"
		}
	))
	end_date = forms.DateTimeField(initial=timezone.now(), widget=forms.TextInput(
		attrs={
			"class": "green white-font"
		}
	))