# Generated by Django 2.1.1 on 2018-10-03 06:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_4', '0005_schedule'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='end_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='start_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
