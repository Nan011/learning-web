from django.conf.urls import url
from django.urls import path
from . import views

urlpatterns = [
	path("profile/", views.get_profile, name="profile"),
	path("projects/", views.get_projects, name="projects"),
	path("home/", views.get_home, name="home"),
	path("register/", views.get_register, name="register"),
	path("schedules/", views.get_schedules, name="schedules"),
	path("schedules/delete_all/", views.delete_all_schedule, name="schedules/delete_all/"),
	path("schedules/delete/<int:id>/", views.delete_schedule),
]