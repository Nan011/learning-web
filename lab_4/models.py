from django.db import models
from django.utils import timezone
from datetime import date

# Create your models here.
TITLE_LENGTH = 100
SHORT_LENGTH = 30
DESC_LENGTH = 300

class Icon(models.Model):
	icon = models.FileField(null=True, upload_to="icons/")
	name = models.CharField(max_length=TITLE_LENGTH)
	def __str__(self):
		return self.name

class Image(models.Model):
	image = models.ImageField(null=True, upload_to="imgs/")
	name = models.CharField(max_length=TITLE_LENGTH)
	def __str__(self):
		return self.name

class AbilityType(models.Model):
	name = models.CharField(max_length=SHORT_LENGTH)
	def __str__(self):
		return self.type
		
class Project(models.Model):
	icon = models.ForeignKey(Icon, on_delete=models.SET_NULL, null=True)
	name = models.CharField(max_length=TITLE_LENGTH)
	type = models.CharField(max_length=SHORT_LENGTH)
	role = models.CharField(max_length=SHORT_LENGTH)
	additional_information = models.CharField(max_length=100, blank=True, null=True)
	year = models.IntegerField(default=timezone.now().year)
	description = models.TextField(max_length=DESC_LENGTH, null=True)
	def __str__(self):
		return self.name

class Education(models.Model):
	instance = models.CharField(max_length=TITLE_LENGTH)
	joined_date = models.DateField("joined date")
	graduated_date = models.DateField("graduated year", blank=True, null=True)
	description = models.TextField(max_length=DESC_LENGTH, blank=True, null=True)
	def __str__(self):
		return self.instance
	class Meta:
		verbose_name_plural = 'Education'


class Ability(models.Model):
	name = models.CharField(max_length=TITLE_LENGTH)
	type = models.ForeignKey(AbilityType, on_delete=models.SET_NULL, null=True)
	value = models.IntegerField(default=1)
	def __str__(self):
		return self.name
	class Meta:
		verbose_name = "Ability"
		verbose_name_plural = 'Abilities'


class Profile(models.Model):
	photo = models.ForeignKey(Image, on_delete=models.SET_NULL, null=True)
	name = models.CharField(max_length=TITLE_LENGTH) 
	birth_date = models.DateField("birth date")
	email = models.CharField(max_length=SHORT_LENGTH)
	description = models.TextField(max_length=DESC_LENGTH, blank=True, null=True)
	def __str__(self):
		return self.name

class Schedule(models.Model):
	name = models.CharField(max_length=TITLE_LENGTH);
	category = models.CharField(max_length=SHORT_LENGTH)
	place = models.CharField(max_length=SHORT_LENGTH)
	start_date = models.DateTimeField(blank=True, null=True)
	end_date = models.DateTimeField(blank=True, null=True)
	def __str__(self):
		return self.name	

class ScheduleCategory(models.Model) :
    HOBBY = "Hobbies"
    BIG_EVENT = "Big Event"
    SMALL_EVENT = "Small Event"
    WORK = "Work"
    OTHER = "Other"
    
    CATEGORY_CHOICES = (
        (HOBBY, "Hobbies"),
        (BIG_EVENT, "Big Event"),
        (SMALL_EVENT, "Small Event"),
        (WORK, "Work"),
        (OTHER, "Other"),
    )

    category = models.CharField(
        max_length = 2,
        choices = CATEGORY_CHOICES,
        default = HOBBY,
    )


