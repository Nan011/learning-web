function adder(toggle) {
	var form = document.getElementsByClassName("form-con")[0];
	var background = document.getElementsByClassName("wall")[0];
	if (toggle === 0) {
		form.classList.remove("form-con-click");
		background.classList.remove("dark-bg");
	} else if (toggle === 1) {
		form.classList.add("form-con-click");
		background.classList.add("dark-bg");
	}	
}